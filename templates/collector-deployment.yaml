---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{include "snowplow-kinesis.fullname" .}}-collector
  labels:
    app.kubernetes.io/name: {{include "snowplow-kinesis.name" .}}-collector
    helm.sh/chart: {{include "snowplow-kinesis.chart" .}}
    app.kubernetes.io/instance: {{.Release.Name}}
    app.kubernetes.io/managed-by: {{.Release.Service}}
spec:
  replicas: {{.Values.collector.replicaCount}}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: {{include "snowplow-kinesis.name" .}}-collector
      app.kubernetes.io/instance: {{.Release.Name}}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{include "snowplow-kinesis.name" .}}-collector
        app.kubernetes.io/instance: {{.Release.Name}}
    spec:
      serviceAccountName: snowplow-kinesis-collector
      automountServiceAccountToken: true
      hostname: {{include "snowplow-kinesis.fullname" .}}
      restartPolicy: Always
      containers:
        - name: {{.Chart.Name}}
          image: "{{ .Values.collector.image.repository }}:{{ .Values.collector.image.tag }}"
          imagePullPolicy: {{.Values.collector.image.pullPolicy}}
          args:
            ["--config", "/snowplow/config/collector/collector-config.hocon"]
          volumeMounts:
            - name: config-volume
              mountPath: /snowplow/config/collector
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
            - name: jmx
              containerPort: 1099
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health
              port: http
              scheme: HTTP
            initialDelaySeconds: 30
            timeoutSeconds: 8
            periodSeconds: 5
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            httpGet:
              path: /health
              port: http
              scheme: HTTP
            initialDelaySeconds: 30
            timeoutSeconds: 8
            periodSeconds: 5
            successThreshold: 1
            failureThreshold: 3
          resources: {{- toYaml .Values.resources | nindent 12}}
      securityContext: # This is because we need to access the /var/run/secrets/eks.amazonaws.com/serviceaccount/token mounted file
        fsGroup: 65534
      volumes:
        - name: config-volume
          configMap:
            name: {{include "snowplow-kinesis.fullname" .}}
