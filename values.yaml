---
# Default values for snowplow-scala-stream-collector-kinesis.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

nameOverride: ""
fullnameOverride: ""

service:
  type: LoadBalancer
  port: 443
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-backend-protocol: http
    service.beta.kubernetes.io/aws-load-balancer-ssl-cert: arn:aws:acm:us-east-1:324044571751:certificate/45b20933-d600-4c7f-9976-754ed343e5c7
    service.beta.kubernetes.io/aws-load-balancer-ssl-ports: "https"

resources:
  {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #  cpu: 100m
  #  memory: 128Mi
  # requests:
  #  cpu: 100m
  #  memory: 128Mi

collector:
  image:
    repository: snowplow/scala-stream-collector-kinesis
    tag: 1.0.1
    pullPolicy: IfNotPresent
  replicaCount: 1

  config: |
    collector {
      interface = "0.0.0.0"
      port = 8080

      paths {
        "/com.minds/t" = "/com.snowplowanalytics.snowplow/tp2"
        "/com.minds/r" = "/r/tp2"
        "/com.minds/i" = "/com.snowplowanalytics.iglu/v1"
      }

      p3p {
        policyRef = "/w3c/p3p.xml"
        CP = "NOI DSP COR NID PSA OUR IND COM NAV STA"
      }

      crossDomain {
        enabled = false
        domains = ["*"]
        secure = true
      }

      cookie {
        enabled = false
        expiration = "365 days"
        name = collectorCookieName
        domain = cookieDomain
        secure = true
        httpOnly = true
      }

      cookieBounce {
        enabled = false
        name = n3pc
        fallbackNetworkUserId = "00000000-0000-4000-A000-000000000000"
        forwardedProtocolHeader = "X-Forwarded-Proto"
      }

      doNotTrackCookie {
        enabled = false
        name = dnt
        value = 1
      }

      rootResponse {
        enabled = false
        statusCode = 200
        body = ok
      }

      redirectMacro {
        enabled = false
        placeholder = "[TOKEN]"
      }

      cors {
        accessControlMaxAge = 10 seconds
      }

      prometheusMetrics.enabled = false

      streams {
        good = "snowplow-raw-good"
        bad = "snowplow-raw-bad"

        useIpAddressAsPartitionKey = false

        sink {
          enabled = kinesis
          region = "us-east-1"
          threadPoolSize = 10

          aws {
            accessKey = default
            secretKey = default
          }

          backoffPolicy {
            minBackoff = 10
            maxBackoff = 300000
          }
        }

        buffer {
          byteLimit = 30720000
          recordLimit = 1
          timeLimit = 0
        }
      }
    }

    akka {
      loglevel = DEBUG
      loggers = ["akka.event.slf4j.Slf4jLogger"]

      http.server {
        remote-address-header = on
        raw-request-uri-header = on
        parsing {
          max-uri-length = 32768
          uri-parsing-mode = relaxed
        }
      }
    }

enrich:
  image:
    repository: snowplow/stream-enrich-kinesis
    tag: 1.3.1
    pullPolicy: IfNotPresent
  replicaCount: 1

  config: |
    enrich {
      production = true

      paths {
        "/com.minds/a" = "/com.snowplowanalytics.snowplow/tp2"
      }

      streams {
        in {
          raw = "snowplow-raw-good"
        }

        out {
          enriched = "snowplow-enriched-good"
          bad = "snowplow-enriched-bad"
          partitionKey = "user_ipaddress"
        }

        sourceSink {
          enabled = kinesis

          aws {
            accessKey = default
            secretKey = default
          }

          region = "us-east-1"
          maxRecords = 10000
          initialPosition = TRIM_HORIZON
          backoffPolicy {
            minBackoff = 10
            maxBackoff = 300000
          }
        }

        buffer {
          byteLimit = 16384
          recordLimit = 1000
          timeLimit = 10000
        }

        appName = "SnowplowEnrich-minds-us-east-1"
      }
    }
  iglu_resolver: |
    {
      "schema": "iglu:com.snowplowanalytics.iglu/resolver-config/jsonschema/1-0-1",
      "data": {
        "cacheSize": 500,
        "repositories": [
          {
            "name": "Iglu Central",
            "priority": 0,
            "vendorPrefixes": [ "com.snowplowanalytics" ],
            "connection": {
              "http": {
                "uri": "http://iglucentral.com"
              }
            }
          },
          {
            "name": "Iglu Central - GCP Mirror",
            "priority": 1,
            "vendorPrefixes": [ "com.snowplowanalytics" ],
            "connection": {
              "http": {
                "uri": "http://mirror01.iglucentral.com"
              }
            }
          },
          {
            "name": "Minds Iglu",
            "priority": 0,
            "vendorPrefixes": [
              "com.minds"
            ],
            "connection": {
              "http": {
                "uri": "https://iglu.minds.io"
              }
            }
          }
        ]
      }
    }
